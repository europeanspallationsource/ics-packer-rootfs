pipeline {
    agent { label 'packer' }
    environment {
        ROOTFS_VERSION = sh(returnStdout: true, script: 'git describe --exact-match || true').trim()
    }

    stages {
        stage('Validate') {
            steps {
                slackSend (color: 'good', message: "STARTED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
                sh 'packer validate rootfs.json'
            }
        }
        stage('Build') {
            steps {
                ansiColor('xterm') {
                    sh 'packer build rootfs.json'
                }
            }
        }
        stage('Upload to artifactory') {
            when {
                not { environment name: 'ROOTFS_VERSION', value: '' }
            }
            steps {
                script {
                    def server = Artifactory.server 'artifactory.esss.lu.se'
                    def uploadSpec = """{
                      "files": [
                        {
                          "pattern": "build/centos7-ess-rootfs-${ROOTFS_VERSION}.tar.bz2",
                          "target": "swi-pkg/EEE/centos/"
                        }
                      ]
                    }"""
                    server.upload(uploadSpec)
                }
            }
        }
    }

    post {
        always {
            /* clean up the workspace */
            deleteDir()
        }
        failure {
            slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
        success {
            slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
    }

}
