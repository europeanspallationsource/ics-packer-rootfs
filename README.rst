Packer template to build the EEE rootfs
=======================================

This Packer_ template creates the rootfs for the EEE server.
The root filesystem is built inside a virtualbox machine (using packer).

The required packages for EPICS development are installed using the ansible role
ics-ans-role-epics-dev-requirements.
New required packages should be added to that role.

Usage
-----

The archive is built automatically and uploaded to artifactory by Jenkins
(you can check the Jenkinsfile).

To build the rootfs archive locally, run::

    $ export ROOTFS_VERSION=$(date +%Y%m%d)
    $ packer build rootfs.json

This will create the archive under build/


.. _Packer: https://www.packer.io
